EESchema Schematic File Version 2
LIBS:hpr-telem-board-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:w_analog
LIBS:sprk_rfm69hcw
LIBS:gy-gps6mv2
LIBS:ESP8266
LIBS:bme280
LIBS:si1401edh
LIBS:hpr-telem-board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 8
Title "HPR Telemetry Module"
Date "2017-03-07"
Rev "0.1"
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 6850 3500
NoConn ~ 6850 3400
Wire Wire Line
	6850 4050 7000 4050
Wire Wire Line
	7000 4050 7000 4450
Wire Wire Line
	6850 4250 7000 4250
Connection ~ 7000 4250
Wire Wire Line
	6850 4150 7250 4150
$Comp
L GND #PWR013
U 1 1 58BF2A5C
P 7000 4450
F 0 "#PWR013" H 7000 4200 50  0001 C CNN
F 1 "GND" H 7000 4300 50  0000 C CNN
F 2 "" H 7000 4450 50  0000 C CNN
F 3 "" H 7000 4450 50  0000 C CNN
	1    7000 4450
	1    0    0    -1  
$EndComp
Text HLabel 7250 4150 2    60   Output ~ 0
ANT
Text HLabel 4400 3100 0    60   Input ~ 0
VDD
Wire Wire Line
	5000 3400 5150 3400
Text HLabel 7250 3800 2    60   Output ~ 0
RF_MISO
Text HLabel 7250 3700 2    60   Input ~ 0
RF_MOSI
Text HLabel 7250 3900 2    60   Input ~ 0
RF_SCK
Text HLabel 7250 3600 2    60   BiDi ~ 0
RF_NSS
Text HLabel 4800 3800 0    60   Output ~ 0
RF_INT
Wire Wire Line
	4800 3800 5150 3800
NoConn ~ 5150 4250
$Comp
L Si1401EDH Q2
U 1 1 58C82F81
P 4800 3000
F 0 "Q2" H 5100 3050 50  0000 R CNN
F 1 "Si1401EDH" H 5425 2950 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SC-70-6_Handsoldering" H 5000 3100 50  0001 C CNN
F 3 "" H 4800 3000 50  0000 C CNN
	1    4800 3000
	0    1    1    0   
$EndComp
$Comp
L R R11
U 1 1 58C83032
P 4500 2950
F 0 "R11" V 4580 2950 50  0000 C CNN
F 1 "100k" V 4500 2950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4430 2950 50  0001 C CNN
F 3 "" H 4500 2950 50  0000 C CNN
	1    4500 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3300 5100 3300
Wire Wire Line
	5100 3100 5100 3400
Connection ~ 5100 3400
Wire Wire Line
	5000 3200 5100 3200
Connection ~ 5100 3300
Wire Wire Line
	5000 3100 5100 3100
Connection ~ 5100 3200
Wire Wire Line
	4400 3100 4600 3100
Connection ~ 4500 3100
Text HLabel 4400 2800 0    60   Input ~ 0
RF_EN
Wire Wire Line
	4400 2800 4800 2800
Connection ~ 4500 2800
Wire Wire Line
	5150 3400 5150 3000
$Comp
L PWR_FLAG #FLG014
U 1 1 58C8D36C
P 5150 3000
F 0 "#FLG014" H 5150 3095 50  0001 C CNN
F 1 "PWR_FLAG" H 5150 3180 50  0000 C CNN
F 2 "" H 5150 3000 50  0000 C CNN
F 3 "" H 5150 3000 50  0000 C CNN
	1    5150 3000
	1    0    0    -1  
$EndComp
$Comp
L SPRK_RFM69HCW U3
U 1 1 58DA9B39
P 6000 3850
F 0 "U3" H 6550 4450 60  0000 C CNN
F 1 "SPRK_RFM69HCW" H 6000 3850 60  0000 C CNN
F 2 "" H 6000 3600 60  0001 C CNN
F 3 "" H 6000 3600 60  0001 C CNN
	1    6000 3850
	1    0    0    -1  
$EndComp
NoConn ~ 5150 3500
NoConn ~ 5150 3700
NoConn ~ 5150 3900
NoConn ~ 5150 4000
Wire Wire Line
	6850 3600 7250 3600
Wire Wire Line
	6850 3700 7250 3700
Wire Wire Line
	6850 3800 7250 3800
Wire Wire Line
	6850 3900 7250 3900
$EndSCHEMATC
